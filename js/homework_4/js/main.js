
let user = createNewUser();

function createNewUser() {
    return {
        _firstName: prompt('Enter first name: '),
        _lastName: prompt('Enter last name: '),
        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
    }
}

console.log(user.getLogin());

Object.defineProperty(user, "firstName", {
    get: function() {
        return this._firstName;
    },
    set: function(value) {
         this._firstName = value;
    }
});

Object.defineProperty(user, "lastName", {
    get: function() {
        return this._lastName;
    },
    set: function(value) {
         this._lastName = value;
    }
});
