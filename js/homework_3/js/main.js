let firstNumber = prompt('Enter first number: ');
let secondNumber = prompt('Enter second number: ');

while (firstNumber === null || firstNumber.trim() ==='' || isNaN(+firstNumber)) {
    firstNumber = prompt('Enter first number: ', firstNumber);
}

while (secondNumber === null || secondNumber.trim() ==='' || isNaN(+secondNumber)) {
    secondNumber = prompt('Enter second number: ', secondNumber);
}

let operation = prompt('Enter the operation: ');
while(!(operation === "+" || operation === '-' || operation === '*' || operation === '/')) {
    operation = prompt('Enter the operation:' );
}


function calculate (firstNumber, secondNumber, operation) {
    if(operation === "*") {
        console.log(+firstNumber * +secondNumber);
    } else if ( operation === "/") {
        console.log(+firstNumber / +secondNumber);
    } else if (operation === "+") {
        console.log(+firstNumber + +secondNumber);
    }else {
        console.log(+firstNumber - +secondNumber);
    }
}

calculate(firstNumber,secondNumber,operation);