// Button Top
const buttonTop = $('.icon-chevron-up');

$(window).scroll(() => {
    if($(window).scrollTop() > window.innerHeight) {
        buttonTop.removeClass('hidden')
    } else {
        buttonTop.addClass('hidden')
    }
    console.log(window.clientHeigh)
})

buttonTop.on('click', () => {
    $('html, body').animate({scrollTop: 0}, 1000);
})

// Animation Block

$(function(){
    $('a[href^="#"]').on('click', function(event) {
        event.preventDefault(); 
        let block = $(this).attr("href"),
            position = $(block).offset().top;
        $('html, body').animate({scrollTop: position}, 1000);
    });
});

// TOGGLE SECTION

const toggleBtn = $('.toggle');

toggleBtn.on('click', slideToggle)

function slideToggle() {
    let section = $('#posts');
    section.find('.wrapper').toggleClass('hidden');
}