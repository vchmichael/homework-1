let taskNumber = 5;

let inputNumber = +prompt('Enter number: ');
let result = 0;

while (!Number.isInteger(inputNumber)) {
    inputNumber = +prompt('Enter integer number: ');
}

if(inputNumber < taskNumber) {
    console.log('Sorry, no numbers');
} else {

for (let i = 1; i <= inputNumber; i++) {
    if (i % taskNumber === 0) {
        result += " " + i;
    } 

}
console.log(result);
}
