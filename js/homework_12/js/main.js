let imgs = document.querySelectorAll('.image-to-show');
let start = document.querySelector('.btn-start');
let stop = document.querySelector('.btn-stop');

imgs.forEach((e) => {
    e.style.display ='none'
})
imgs.item(0).style.display = 'inline'

let i = 1;
function showImages() {
    imgs[i-1].style.display ='none'
    if(i >=imgs.length) {
        i = 0;
    }
    imgs[i].style.display ='inline'
    i++
}

let timerId = setInterval(showImages, 10000)

start.addEventListener('click', () => {
    timerId = setInterval(showImages, 10000)

});

stop.addEventListener('click', () => { 
    clearInterval(timerId)  
})


