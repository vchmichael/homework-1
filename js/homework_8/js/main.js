let input = document.querySelector('.price-input');
let spanBlock = document.querySelector('.span-block')
let info = document.createElement('span');
info.style.display='none'
spanBlock.append(info);


    input.addEventListener('blur', () => {
        let price = input.value;
        if (price > 0) {
            info.style.display = 'inline'
            info.innerHTML = `Текущая цена:${price} <i class="icon-remove-sign"></i>`;
            input.style.color="green"
            if (priceInput.classList.contains('wrong')) {
                priceInput.classList.remove('wrong')
                document.body.childNodes[1].lastChild.remove();
            }
            info.lastChild.addEventListener('click', () => {
                info.style.display='none'
                price= input.value="";
            })
        } 
        else if (!(priceInput.classList.contains('wrong'))) {
            priceInput.classList.add('wrong')
            document.querySelector('.main').append('Please enter correct price');
            input.style.color="red"
        }
    })



let priceInput = document.querySelector('.price-input');


    input.addEventListener('focus', () => {
        priceInput.classList.add('focused')
    })
    input.addEventListener('blur', () => {
        priceInput.classList.toggle('focused')
    })

