let tab = document.querySelectorAll('.tabs li');
let content = document.querySelectorAll('.tabs-content li');


for(let i = 0; i < tab.length; i++){
    tab[i].classList.add('tabs-title');
    tab[i].addEventListener('click',function(){
      for( let u = 0; u < tab.length; u++){
        content[u].style.display = 'none';
        tab[u].classList.remove('active');
      }
      content[i].style.display = 'block';
      this.classList.add('active');
    });
  }

