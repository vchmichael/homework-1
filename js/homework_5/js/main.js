//Create New User

let user = createNewUser();

function createNewUser() {
    return {
        _firstName: prompt('Enter first name: '),
        _lastName: prompt('Enter last name: '),
        birthday: prompt('Enter your birthday(dd.mm.yyyy)'),
        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
        getAge() {
            let age = new Date() - new Date(+this.birthday.slice(6), +(this.birthday.slice(3, 5)-1), +this.birthday.slice(0, 2));
            return Math.floor(age / 3.154e+10);
        },
        getPassword() {
            return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this.birthday.slice(6);
        },
    }
}

// END create User

  console.log (user);
  console.log(user.getLogin());
  console.log(user.getAge());
  console.log(user.getPassword());

//User Fullname - Setter & Getter

Object.defineProperty(user, "firstName", {
    get: function() {
        return this._firstName;
    },
    set: function(value) {
         this._firstName = value;
    }
});

Object.defineProperty(user, "lastName", {
    get: function() {
        return this._lastName;
    },
    set: function(value) {
         this._lastName = value;
    }
});

//END User Fullname - Setter & Getter
