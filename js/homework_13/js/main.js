let header = document.querySelector('.main-menu');
let mainContent = document.querySelector('.main-content');
let title = document.querySelector('.main-tittle');
let mainContainer = document.querySelector('.main');
let body = document.querySelector('body');
let allTexts = document.querySelectorAll('.main-paragraph, span');

// Save Theme after reload page

window.onload = () => {
if (localStorage.getItem('darkTheme') === 'true') {
    changeThemeToDark()
} else {
    changeThemeToLight()
}
};

//BUTTON CHANGE THEME COLORS

let btn = document.querySelector('.btn-theme');

btn.addEventListener('click', () => {
if (localStorage.getItem('darkTheme') === 'true') {
    changeThemeToLight()
} else {
    changeThemeToDark()
}
});


function changeThemeToDark() {
    //Set boolean into localStorage
    localStorage.setItem('darkTheme', true)

    //Change color of main elements
    header.style.background = '#241F34'
    mainContent.style.background = '#282144'
    title.style.color = '#CFCFCF'
    mainContainer.style.background = '#282144'
    body.style.background = '#241F34'
    allTexts.forEach((paragrapg) => {
        paragrapg.style.color = "#fff"
    })

}

function changeThemeToLight() {
    //Set boolean into localStorage
    localStorage.setItem('darkTheme', false)

    //Change color of main elements
    header.style.background = '#fff'
    mainContent.style.background = '#F5F6F7'
    title.style.color = '#000'
    mainContainer.style.background = '#F5F6F7'
    body.style.background = '#fff'
    allTexts.forEach((paragrapg) => {
        paragrapg.style.color = "#000"
    })
};